using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using IniParser;
using IniParser.Model;
using System.Threading;
using NationalInstruments.DAQmx;

namespace DAQ_Writer
{
    class Program
    {
        static private IniData IniData;

        static private SerialPort _serialPort;

        static private int _timeBetweenSignals;

        static string Lines;
        static string DeviceName;

        static void Main(string[] args)
        {
            Program p = new Program();
            _serialPort = new SerialPort();

            var parser = new FileIniDataParser();
            IniData = parser.ReadFile("daq-settings.ini");
            p.PopulateDeviceInformation();

            _timeBetweenSignals = int.Parse(IniData["Comm"]["TimeBetweenSignals"]);

            //int debug = int.Parse(IniData["Settings"]["Debug"]);

            bool closeSignalReceived = false;

            try
            {
                //if(debug == 0) _serialPort.Open();
            } 
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine(e.GetType().ToString() + " Access is denied to the port OR The current process, or another process on the system, already has the specified COM port open either by a SerialPort instance or in unmanaged code.");
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.GetType().ToString() + " One or more of the properties for this instance are invalid. For example, the Parity, DataBits, or Handshake properties are not valid values; the BaudRate is less than or equal to zero; the ReadTimeout or WriteTimeout property is less than zero and is not InfiniteTimeout.");
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.GetType().ToString() + " The port name does not begin with \"COM\" or The file type of the port is not supported.");
            }
            catch (IOException e)
            {
                Console.WriteLine(e.GetType().ToString() + " The port is in an invalid state or An attempt to set the state of the underlying port failed. For example, the parameters passed from this SerialPort object were invalid.");
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.GetType().ToString() + " The specified port on the current instance of the SerialPort is already open.");
            }

            while(!closeSignalReceived)
            {
                var input = Console.ReadLine();
                if (input == "0")
                {
                    Console.WriteLine("Read a 0");
                    closeSignalReceived = true;
                }
                else if (input == "1")
                {
                    Console.WriteLine("Read a 1");

                    p.WriteOne();
                    Console.WriteLine("Waiting");
                    Thread.Sleep(_timeBetweenSignals);
                    p.WriteZero();
                    Console.WriteLine("Waiting");
                    Thread.Sleep(_timeBetweenSignals);
                }
            }
        }

        public void WriteZero()
        {
            using (NationalInstruments.DAQmx.Task digitalWriteTask = new NationalInstruments.DAQmx.Task())
            {
                digitalWriteTask.DOChannels.CreateChannel(Lines, DeviceName,
                    ChannelLineGrouping.OneChannelForAllLines);

                try
                {
                    DigitalSingleChannelWriter writer = new DigitalSingleChannelWriter(digitalWriteTask.Stream);
                    writer.WriteSingleSamplePort(true, (UInt32)0);
                }
                catch (DaqException dex)
                {
                    string path = Directory.GetCurrentDirectory() + @"\daqlog.txt";
                    System.IO.File.WriteAllText(path, dex.Message);
                }
            }
            /*
            try
            {
                _serialPort.Write(new byte[1] { 48 }, 0, 1);
                Console.WriteLine("Wrote 0");
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine(e.GetType().ToString() + " The buffer passed is null.");
            }
            catch(InvalidOperationException e)
            {
                Console.WriteLine(e.GetType().ToString() + " The specified port is not open.");
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.GetType().ToString() + " The offset or count parameters are outside a valid region of the buffer being passed. Either offset or count is less than zero.");
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.GetType().ToString() + " offset plus count is greater than the length of the buffer.");
            }
            catch(TimeoutException e)
            {
                Console.WriteLine(e.GetType().ToString() + " The operation did not complete before the time-out period ended.");
            }
            */
        }

        public void WriteOne()
        {
            using (NationalInstruments.DAQmx.Task digitalWriteTask = new NationalInstruments.DAQmx.Task())
            {
                digitalWriteTask.DOChannels.CreateChannel(Lines, DeviceName,
                    ChannelLineGrouping.OneChannelForAllLines);

                try
                {
                    DigitalSingleChannelWriter writer = new DigitalSingleChannelWriter(digitalWriteTask.Stream);
                    writer.WriteSingleSamplePort(true, (UInt32)1);
                }
                catch (DaqException dex)
                {
                    string path = Directory.GetCurrentDirectory() + @"\daqlog.txt";
                    System.IO.File.WriteAllText(path, dex.Message);
                }
            }

            /*
            try
            {
                _serialPort.Write(new byte[1] { 49 }, 0, 1);
                Console.WriteLine("Wrote 1");
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine(e.GetType().ToString() + " The buffer passed is null.");
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.GetType().ToString() + " The specified port is not open.");
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.GetType().ToString() + " The offset or count parameters are outside a valid region of the buffer being passed. Either offset or count is less than zero.");
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.GetType().ToString() + " offset plus count is greater than the length of the buffer.");
            }
            catch (TimeoutException e)
            {
                Console.WriteLine(e.GetType().ToString() + " The operation did not complete before the time-out period ended.");
            }
            */
        }

        public void PopulateDeviceInformation()
        {
            /*
            _serialPort.PortName = SetPortName(IniData["Settings"]["PortName"]);
            _serialPort.BaudRate = SetBaudRate(int.Parse(IniData["Settings"]["BaudRate"]));
            _serialPort.Parity = SetParity(IniData["Settings"]["ParityBits"]);
            _serialPort.DataBits = SetDataBits(int.Parse(IniData["Settings"]["DataBits"]));
            _serialPort.StopBits = SetStopBits(float.Parse(IniData["Settings"]["StopBits"]));
            _serialPort.Handshake = SetHandShake(IniData["Settings"]["FlowControl"]);
            */

            Lines = "Dev" + IniData["Comm"]["DeviceNumber"] + "/port" + IniData["Comm"]["Port"];
            DeviceName = "port" + IniData["Comm"]["DeviceNumber"];
        }

        private string SetPortName(string portName)
        {
            return portName;
        }

        private int SetBaudRate(int baudrate)
        {
            return baudrate;
        }

        private Parity SetParity(string parity)
        {
            foreach(var p in Enum.GetNames(typeof(Parity)))
            {
                if(string.Equals(p, parity,StringComparison.CurrentCultureIgnoreCase))
                {
                    return (Parity)Enum.Parse(typeof(Parity), p, true);
                }
            }

            return Parity.None;
        }

        private int SetDataBits(int d)
        {
            return d;
        }

        private StopBits SetStopBits(float s)
        {
            if(s == 0) return StopBits.None;
            if(s == 1.0f) return StopBits.One;
            if(s == 1.5f) return StopBits.OnePointFive;
            if(s == 2.0f) return StopBits.Two;

            return StopBits.None;
        }

        private Handshake SetHandShake(string handshake)
        {
            foreach(var h in Enum.GetNames(typeof(Handshake)))
            {
                if(string.Equals(h, handshake, StringComparison.CurrentCultureIgnoreCase))
                {
                    return (Handshake)Enum.Parse(typeof(Handshake), h, true);
                }
            }

            return Handshake.None;
        }
    }
}
